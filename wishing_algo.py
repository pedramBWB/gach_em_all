"""
Script de calcul de chance d'obtention d'un personnage de différente rareté 
"""
import random
import json

characters_json = open("characters_test.json")
characters_list = json.load(characters_json)

rarities = [
    {"id": 1, "name": "Bronze", "weight": 5000},
    {"id": 2, "name": "Argent", "weight": 3500},
    {"id": 3, "name": "Or", "weight": 2000},
    {"id": 4, "name": "Platine", "weight": 1500},
    {"id": 5, "name": "Diamant", "weight": 1000},
    {"id": 6, "name": "Emeraude", "weight": 500},
    {"id": 7, "name": "Lapis", "weight": 200},
    {"id": 8, "name": "Diamant Rouge", "weight": 50},
]

totalRarityWeight = 0
totalCharactersWeight = 0 

def calculateRarityWeight():
    global totalRarityWeight
    for rarity in rarities:
        totalRarityWeight += rarity["weight"]
        
def calculateCharactersWeight():
    global totalCharactersWeight
    for weight in characters_list["characters_list"]:
        totalCharactersWeight += weight["weight"]
        
calculateCharactersWeight()
calculateRarityWeight()

def generateRandomNum():
    randomNum = random.randint(0, totalRarityWeight)
    return randomNum

droppedRarity = []
droppedCharacters = []

def generateRarity():   
    rngNum = generateRandomNum()  
    for rarity in rarities:
        if rngNum <= rarity["weight"]:
            droppedRarity.append(rarity["name"])
            break
        else:
            rngNum -= rarity["weight"]
            
def generateCharacter(rarityList, i):
    sameRarityCharacters = []
    for character in characters_list["characters_list"]:
        if character["rarity"] == rarityList[i]:
            sameRarityCharacters.append(character) 
    chosenCharacter = random.choice(sameRarityCharacters)
    droppedCharacters.append(chosenCharacter)

def makeWish(wish):
    for i in range(wish): 
        generateRarity()
        generateCharacter(droppedRarity, i)
    return droppedCharacters
        
print(makeWish(10))
                
    

